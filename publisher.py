'''
Script to publish an OTA update to devices
'''

import json

import paho.mqtt.client as mqtt

client_name = "sys_update_multicast"
client = mqtt.Client(client_name)

host_name = "localhost"
client.connect(host_name, 1883, 60)

payload = {
    "ota_version": "1.0.1",
    "ota_download_link": "https://ap-firmware-update.s3.ap-south-1.amazonaws.com/input.hex",
    "filename": "",
    "package_hash": "",
    "script_hash": "",
    "force": False
}

payload_string = json.dumps(payload)

client.publish("/mqtt_client/firmware_update", payload_string)
