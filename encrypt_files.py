import os
from base64 import b64decode
from Crypto.Hash import CMAC
from Crypto.Cipher import AES
import gnupg

KEYFILE = os.expanduser('ubuntu') + "/public.key"
pubkey = open(KEYFILE, 'r').read()

gpg = gnupg.GPG()
import_result = gpg.import_keys(pubkey)

FILENAME = os.expanduser('ubuntu') + "/sysupgrade-image.tar.gz"
data = open(FILENAME, 'rb').read()

status = gpg.encrypt(data, 
                     recipients="meshnetelectronics@gmail.com",
                     symmetric='AES256',
                     passphrase='',
                     armor=False,
                     output=FILENAME+'.enc')

print("Ok: ", status.ok)
print("Status: ", status.status)
print("stderr: ", status.stderr)

if status.ok:
    print("Sysupgrade package encrypted.")

print("Computing hashes...  ")

print("Secret key for hashing is: ", os.environ['SECRET_KEY'])
secret_key = b64decode(os.environ['SECRET_KEY'].encode('utf-8'))
upgrade_obj = CMAC.new(secret_key, ciphermod=AES)
upgrade_obj.update(open(os.expanduser('ubuntu') + "/upgrade_firmware.py", "rb").read())
signature = upgrade_obj.hexdigest()

print("Upgrade file hash: ", signature)

package_obj = CMAC.new(secret_key, ciphermod=AES)
package_obj.update(open(os.expanduser('ubuntu') + "/sysupgrade-image.tar.gz", "rb").read())
signature = package_obj.hexdigest()

print("Package file hash: ", signature)
