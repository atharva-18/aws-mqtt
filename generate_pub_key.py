import os
from base64 import b64encode

key = os.urandom(32)

print("32 bytes secret key: ", b64encode(key).decode('utf-8'))
