'''
Device script
'''

import json
import os
from base64 import b64decode
from Crypto.Hash import CMAC
from Crypto.Cipher import AES
import gnupg
from packaging import version
import requests

import paho.mqtt.client as mqtt
from uci import Uci

def on_ota_update(client, obj, msg):
    print("OTA update received on topic: ", msg.topic)
    u = Uci()
    KEYFILE = u.get("upgrade", "updateconf", "keyfile")
    SECRET_KEY = u.get("upgrade", "updatrconf", "secret_key")
    CURR_VERSION = u.get("upgrade", "updateconf", "version")

    data = json.loads(msg.payload.decode("utf-8"))
    UPDATE_VERSION = data["ota_version"]
    URI = data["ota_download_link"]
    FILENAME = data["filename"]
    SHA512_PACKAGE_HASH = data["package_hash"]
    SHA512_SCRIPT_HASH = data["script_hash"]
    FORCE_UPDATE = data["force"]

    if FORCE_UPDATE or version.parse(CURR_VERSION) < version.parse(UPDATE_VERSION):
        filedata = requests.get(URI, allow_redirects=True)
        file_location = u.get("upgrade", "updateconf", "directory")
        os.system("mkdir -p " + file_location + '/')
        open(file_location + '/' + FILENAME + '.enc', 'wb').write(filedata.content)

        # Decrypt and verify signature (internally)
        private_key = open(KEYFILE, 'rb').read()

        gpg = gnupg.GPG()
        import_result = gpg.import_keys(private_key)
        print("Imported key(s): ", import_result.results)

        data = open(FILENAME + '.enc', 'rb').read()
        decrypt_passphrase = u.get("upgrade", "updateconf", "passphrase")
        decrypt_status = gpg.decrypt(data, passphrase=decrypt_passphrase, output=FILENAME)
        
        if decrypt_status.ok:
            print("File decrypted successfully")
            os.system(f"cd {file_location} && tar -xvf {file_location + '/' + FILENAME}")

            secret_key = b64decode(SECRET_KEY.encode('utf-8'))
            cobj = CMAC.new(secret_key, ciphermod=AES)
            cobj.update(open(f"{file_location}/upgrade_firmware.py", "rb").read())
            signature = cobj.hexdigest()
            if signature == SHA512_SCRIPT_HASH:
                os.system(f"cd {file_location} && python3 ./upgrade_firmware.py {FILENAME} {SHA512_PACKAGE_HASH} {SECRET_KEY}")

def on_message(client, obj, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))

def main():
    u = Uci()
    uuid = u.get("device", "uuidconf", "uuid")
    client = mqtt.Client(uuid)

    topic_name = u.get("upgrade", "updateconf", "topic")
    client.message_callback_add('/' + str(uuid) + '/' + topic_name, on_ota_update)
    client.on_message = on_message

    host_name = u.get("upgrade", "updateconf", "hostname")
    client.connect(host_name, 1883, 60)
    client.publish("/device/uuid", payload=str(uuid))
    client.subscribe('/' + str(uuid) + '/' + topic_name, 0)
    client.loop_forever()

if __name__ == "__main__":
    main()
