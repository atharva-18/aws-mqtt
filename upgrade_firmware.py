import argparse
import os
import sys
from Crypto.Hash import CMAC
from Crypto.Cipher import AES

import gnupg # python3-gnupg

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file_name")
    parser.add_argument("sha_hash")
    parser.add_argument("secret_key")
    args = parser.parse_args()

    file_name = args.file_name
    sha_hash = args.sha_hash
    secret_key = args.secret_key.encode('ASCII')
    cobj = CMAC.new(secret_key, ciphermod=AES)
    cobj.update(open(file_name, "r").read())
    signature = cobj.hexdigest()
    
    if signature == sha_hash:
        print("File integrity verified")
        print("Upgrading system firmware")
        ret_code = os.system("sysupgrade -v " + file_name)

        if ret_code != 0:
            print("System upgrade failed")

if __name__ == "__main__":
    main()
